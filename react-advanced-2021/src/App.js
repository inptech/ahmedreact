import React from 'react'
import Setup from './tutorial/ReactMemo/index';
function App() {
  return (
    <div className='container'>
      <Setup />
    </div>
  )
}

export default App